using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChunk : MonoBehaviour {

    [Tooltip("Reference to levelLayoutGenerator (only needs to be assigned for first chunk)")]
    public LevelLayoutGenerator levelLayoutGenerator;

    private BoxCollider triggerCol;

    private void Start() {

        triggerCol = GetComponent<BoxCollider>();

    }

    private void OnTriggerEnter(Collider other) {

        triggerCol.enabled = false;
        levelLayoutGenerator.CheckForSpawn();

    }
}
