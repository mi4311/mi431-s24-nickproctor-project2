using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody _rigidBody;

    [SerializeField, Tooltip("How much acceleration is applied to this " +
        "object when directional input is received.")]
    private float _movementAcceleration = 2;

    [SerializeField, Tooltip("The maximum velocity of this object - keeps " +
        "the player from moving too fast.")]
    private float _movementVelocityMax = 2;

    [SerializeField, Tooltip("Deceleration when no direction input is " +
    "received.")]
    private float _movementFriction = 0.1f;

    [SerializeField, Tooltip("Upwards force applied when jump key is " +
    "pressed.")]
    private float _jumpVelocity = 20;

    [SerializeField, Tooltip("Additional gravitational pull.")]
    private float _extraGravity = 40;

    [SerializeField, Tooltip("The direction the player is facing.")]
    Vector3 _curFacing = Vector3.zero;


    // Start is called before the first frame update
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        // get the current speed from the RigidBody physics component
        // grabbing this ensures we retain gravity speed
        Vector3 curSpeed = _rigidBody.velocity;

        // check to see if any of the keyboard arrows are being pressed
        // if so, adjust the speed of the player
        if (Input.GetKey(KeyCode.RightArrow))
            curSpeed.x += (_movementAcceleration * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftArrow))
            curSpeed.x -= (_movementAcceleration * Time.deltaTime);

        if (Input.GetKey(KeyCode.UpArrow))
            curSpeed.z += (_movementAcceleration * Time.deltaTime);

        if (Input.GetKey(KeyCode.DownArrow))
            curSpeed.z -= (_movementAcceleration * Time.deltaTime);


        // store current facing
        // do this after speed is adjusted by arrows keys, but before friction is applied
        if (curSpeed.x != 0 && curSpeed.z != 0)
            _curFacing = curSpeed.normalized;


        // if both left and right keys are simultaneously pressed (or not pressed),
        // apply friction
        if (Input.GetKey(KeyCode.LeftArrow) == Input.GetKey(KeyCode.RightArrow))
        {
            curSpeed.x -= (_movementFriction * curSpeed.x);
        }

        // apply similar friction logic to up and down keys
        if (Input.GetKey(KeyCode.UpArrow) == Input.GetKey(KeyCode.DownArrow))
        {
            curSpeed.z -= (_movementFriction * curSpeed.z);
        }

        // does the player want to jump?
        if (Input.GetKeyDown(KeyCode.Space) && Mathf.Abs(curSpeed.y) < 1)
            curSpeed.y += _jumpVelocity;
        else
            curSpeed.y -= _extraGravity * Time.deltaTime;

        // apply the max speed
        curSpeed.x = Mathf.Clamp(curSpeed.x,
                            _movementVelocityMax * -1,
                            _movementVelocityMax);

        curSpeed.z = Mathf.Clamp(curSpeed.z,
                            _movementVelocityMax * -1,
                            _movementVelocityMax);

        // apply the changed velocity to this object's physics component
        _rigidBody.velocity = curSpeed;
    }

}
