using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLayoutGenerator : MonoBehaviour
{
    [Tooltip("The level chunk SOs")]
    [SerializeField] private LevelChunkData[] levelChunkData;

    [Header("Track generation options")]
    [Tooltip("How many chunks to spawn on start")]
    [SerializeField] private int chunksToSpawn = 10;

    [Tooltip("Whether or not the track will have infinite generation")]
    [SerializeField] private bool infinite;

    [Tooltip("Check to generate a looping track")]
    private bool loopTrack = false;

    [Header("First chunk settings")]
    [SerializeField] private LevelChunkData firstChunkSO;
    [SerializeField] private GameObject firstChunk;

    [Tooltip("Where the first chunk is located")]
    [SerializeField] private Vector3 spawnPosition;
    [SerializeField] private Vector3 spawnOrigin;

    [Header("Chunk data")]
    public List<Vector3> chunkPositions;
    public List<GameObject> levelChunks;

    // Private variables
    private Vector3 nextChunkPosition;
    private Vector3 nextChunkPosition2;
    private Vector3 nextChunkPosition3;
    private LevelChunkData previousChunk;
    private LevelChunkData finalChunk;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T) && (!loopTrack))
        {
            PickAndSpawnChunk();
        }
    }


    void Start()
    {

        // Add the first chunk's data to the chunk data lists
        previousChunk = firstChunkSO;
        levelChunks.Add(firstChunk);
        chunkPositions.Add(spawnPosition);

        if (!loopTrack)
        {
            // If the number of chunks is < 5, player will see end of track. Set chunksToSpawn to 5 
            if (chunksToSpawn < 5)
            {
                Debug.Log("starting chunks too low, spawning min (5) chunks");
                chunksToSpawn = 5;
            }

            for (int i = 0; i < chunksToSpawn; i++)
            {
                PickAndSpawnChunk();
            }
        }
        else
        {





        }

    }


    LevelChunkData PickNextChunk()
    {

        List<LevelChunkData> allowedChunkList = new List<LevelChunkData>();

        LevelChunkData nextChunk = null;

        LevelChunkData.Direction nextRequiredDirection = LevelChunkData.Direction.North;

        switch (previousChunk.exitDirection)
        {

            case LevelChunkData.Direction.North:
                nextRequiredDirection = LevelChunkData.Direction.South;
                spawnPosition = spawnPosition + new Vector3(0f, 0, previousChunk.chunkSize.y);
                break;

            case LevelChunkData.Direction.East:
                nextRequiredDirection = LevelChunkData.Direction.West;
                spawnPosition = spawnPosition + new Vector3(previousChunk.chunkSize.x, 0, 0);
                break;

            case LevelChunkData.Direction.South:
                nextRequiredDirection = LevelChunkData.Direction.North;
                spawnPosition = spawnPosition + new Vector3(0, 0, -previousChunk.chunkSize.y);
                break;

            case LevelChunkData.Direction.West:
                nextRequiredDirection = LevelChunkData.Direction.East;
                spawnPosition = spawnPosition + new Vector3(-previousChunk.chunkSize.x, 0, 0);
                break;

            default:
                break;

        }

        // Checks to see if there is a chunk 1-3 tiles ahead of each track piece
        // If there is a chunk 1-3 tiles ahead, it will not allow this track piece
        // Will not be added to the list of possible pieces chosen from below
        for (int i = 0; i < levelChunkData.Length; i++)
        {
            if (levelChunkData[i].entryDirection == nextRequiredDirection)
            {

                // Check if the next three chunk spots in exit direction are free
                switch (levelChunkData[i].exitDirection)
                {

                    case LevelChunkData.Direction.North:
                        nextChunkPosition = spawnPosition + new Vector3(0f, 0, levelChunkData[i].chunkSize.y * 1);
                        nextChunkPosition2 = spawnPosition + new Vector3(0f, 0, levelChunkData[i].chunkSize.y * 2);
                        nextChunkPosition3 = spawnPosition + new Vector3(0f, 0, levelChunkData[i].chunkSize.y * 3);
                        if ((!chunkPositions.Contains(nextChunkPosition)) && (!chunkPositions.Contains(nextChunkPosition2)) &&
                            (!chunkPositions.Contains(nextChunkPosition3)))
                        {
                            allowedChunkList.Add(levelChunkData[i]);
                        }
                        break;

                    case LevelChunkData.Direction.East:
                        nextChunkPosition = spawnPosition + new Vector3(levelChunkData[i].chunkSize.x * 1, 0, 0);
                        nextChunkPosition2 = spawnPosition + new Vector3(levelChunkData[i].chunkSize.x * 2, 0, 0);
                        nextChunkPosition3 = spawnPosition + new Vector3(levelChunkData[i].chunkSize.x * 3, 0, 0);
                        if ((!chunkPositions.Contains(nextChunkPosition)) && (!chunkPositions.Contains(nextChunkPosition2)) &&
                            (!chunkPositions.Contains(nextChunkPosition3)))
                        {
                            allowedChunkList.Add(levelChunkData[i]);
                        }
                        break;

                    case LevelChunkData.Direction.South:
                        nextChunkPosition = spawnPosition + new Vector3(0, 0, -levelChunkData[i].chunkSize.y * 1);
                        nextChunkPosition2 = spawnPosition + new Vector3(0, 0, -levelChunkData[i].chunkSize.y * 2);
                        nextChunkPosition3 = spawnPosition + new Vector3(0, 0, -levelChunkData[i].chunkSize.y * 3);
                        if ((!chunkPositions.Contains(nextChunkPosition)) && (!chunkPositions.Contains(nextChunkPosition2)) &&
                            (!chunkPositions.Contains(nextChunkPosition3)))
                        {
                            allowedChunkList.Add(levelChunkData[i]);
                        }
                        break;

                    case LevelChunkData.Direction.West:
                        nextChunkPosition = spawnPosition + new Vector3(-levelChunkData[i].chunkSize.x * 1, 0, 0);
                        nextChunkPosition2 = spawnPosition + new Vector3(-levelChunkData[i].chunkSize.x * 2, 0, 0);
                        nextChunkPosition3 = spawnPosition + new Vector3(-levelChunkData[i].chunkSize.x * 3, 0, 0);
                        if ((!chunkPositions.Contains(nextChunkPosition)) && (!chunkPositions.Contains(nextChunkPosition2)) &&
                            (!chunkPositions.Contains(nextChunkPosition3)))
                        {
                            allowedChunkList.Add(levelChunkData[i]);
                        }
                        break;

                    default:
                        break;
                }
            }

        }

        nextChunk = allowedChunkList[Random.Range(0, allowedChunkList.Count)];

        return nextChunk;

    }


    public void PickAndSpawnChunk()
    {
        LevelChunkData chunkToSpawn = PickNextChunk();

        if (chunkToSpawn != null)
        {
            GameObject objectFromChunk = chunkToSpawn.levelChunks[Random.Range(0, chunkToSpawn.levelChunks.Length)];
            previousChunk = chunkToSpawn;
            GameObject newChunk = Instantiate(objectFromChunk, spawnPosition + spawnOrigin, Quaternion.identity);

            levelChunks.Add(newChunk);
            chunkPositions.Add(spawnPosition);
            objectFromChunk.GetComponent<LevelChunk>().levelLayoutGenerator = this;
        }
    }

    public void UpdateSpawnOrigin(Vector3 originDelta)
    {
        spawnOrigin = spawnOrigin + originDelta;
    }


    public void CheckForSpawn()
    {

        if (infinite)
        {
            PickAndSpawnChunk();
        }
    }

}
